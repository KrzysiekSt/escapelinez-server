const EventEmitter = require('events');
const StartGame = new EventEmitter();
const room = require('./room.js');


let queue = new Array();
let Rooms = new Array();

function addPlayer(player)
{
    queue.push(player);
    if (queue.length % 4 == 0)
    {
        let toRoom;
        StartGame.emit('start');
        for(let i = 0; i < 4; i++)
        {
            toRoom[i] = queue.shift();
            toRoom[i].State = toRoom[i].states[1];
        }
        let roomToAdd = new room.Room(toRoom);

        do {
            roomToAdd.id = Math.floor(Math.random()*1000000);
            let NoId = true;
            for(let i = 0; i < Rooms.length; i++)
            {
                if (Rooms[i].id == roomToAdd.id)
                NoId = false;
            }
        } while (!NoId);//istnieje takie id

        Rooms.push(roomToAdd);
    }
}


module.exports =
{
    add: addPlayer,
    GameEmiter: StartGame
};
