let objects = require("./objects.js");

// class Player
// {
// 	constructor(socket_players, socket_lines, socket_time)
// 	{
// 		this.nick = "";
// 		this.ip = socket_players.remoteAddress;
// 		this.positon = [null, null];
// 		this.socket_players = socket_players;
// 		this.socket_lines = socket_lines;
// 		this.socket_time = socket_time;
// 	}
// }

class Room
{
	constructor(players)
	{
		this.id = null;
		this.Players = players; //player.js
		this.Satrt_places = [[900, 1050], [930, 1050], [990, 1050], [1020, 1050]];
		this.MapSize = [1920, 2160];
		this.Map = new Array(this.MapSize[0]);
		for (let i = 0; i < this.Map.length; i++)
		{
			this.Map[i] = new Array(this.MapSize[1]);
		}
		for (let i = 0; i < this.Map.length; i++)
		{
			for (let j = 0; j < this.Map[i].length; j++)
			{
				this.Map[i][j] = false;
			}
		}
		this.prime = 90;
		this.space = 55;
		this.LinesTable = new Array(36);
		for (let i = 0; i < this.LinesTable.length; i++)
		{
			this.LinesTable[i] = new Array(3);
		}
	}

	linesGeneration()
	{
		for(let i = 0; i < this.LinesTable[i].length; i++) //all levels of lines
		{
		    for(let j = 0; j < this.LinesTable[i][j].length; j++) //lines in level
		    {
				this.LinesTable[i][j] = new LineOb(90+(i*55));
		    }
		}
	}

	sendLines()
	{
		for(let i = 0; i < this.Players.length; i++)
			this.Players[i].socket.write(JSON.stringify(this.LinesTable));
	}

	init()
	{
		if(this.Players[2] == null)
		{
			this.Players[0].socket_players.write
			(`
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}.
			`);
			this.Players[1].socket_players.write
			(`
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}|
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}.
			`);
		}
		else if(this.Players[3] == null)
		{
			this.Players[0].socket_players.write
			(`
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}|
				3|${this.Satrt_places[2][0]}|${this.Satrt_places[2][1]}.

			`);
			this.Players[1].socket_players.write
			(`
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}|
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				3|${this.Satrt_places[2][0]}|${this.Satrt_places[2][1]}.
			`);
			this.Players[2].socket_players.write
			(`
				3|${this.Satrt_places[2][0]}|${this.Satrt_places[2][1]}|
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}.
			`);
		}
		else
		{
			this.Players[0].socket_players.write
			(`
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}|
				3|${this.Satrt_places[2][0]}|${this.Satrt_places[2][1]}|
				4|${this.Satrt_places[3][0]}|${this.Satrt_places[3][1]}.
			`);
			this.Players[1].socket_players.write
			(`
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}|
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				3|${this.Satrt_places[2][0]}|${this.Satrt_places[2][1]}|
				4|${this.Satrt_places[3][0]}|${this.Satrt_places[3][1]}.
			`);
			this.Players[2].socket_players.write
			(`
				3|${this.Satrt_places[2][0]}|${this.Satrt_places[2][1]}|
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}|
				4|${this.Satrt_places[3][0]}|${this.Satrt_places[3][1]}.
			`);
			this.Players[3].socket_players.write
			(`
				4|${this.Satrt_places[3][0]}|${this.Satrt_places[3][1]}|
				1|${this.Satrt_places[0][0]}|${this.Satrt_places[0][1]}|
				2|${this.Satrt_places[1][0]}|${this.Satrt_places[1][1]}|
				3|${this.Satrt_places[2][0]}|${this.Satrt_places[2][1]}.
			`);
		}

		for (let i = 1; i < 6; i++) //countdown before game start
		{
			setTimeout( function ()
			{
				for (let i = 0; i < this.Players.length; i++)
					this.Players[i].socket_time.write(`${i}`);
			}, 1000);
		}

		setTimeout( function ()
		{
			for (let i = 0; i < this.Players.length; i++)
				this.Players[i].socket_time.write(`START`);
		}, 5010);
	}


}

module.exports =
{
	room: Room
};
