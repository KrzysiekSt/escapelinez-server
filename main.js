const net = require('net');
const DB = require('./DB.js');
const util = require('util');
const objects = require("./objects.js");
const EventEmitter = require('events');
const MatchMaking = require('./MatchMaking.js');
const JsonToSend = require('./JsonToSend.js');
const dgram = require('dgram');

const LoginServer = net.createServer();
//-----------------Servers before game ------------------
const nrPlayersServer = dgram.createSocket('udp4');
const switchRoomServer = net.createServer();
//-----------------Servers in game ------------------
//----------------other----------------------
const PlayersConnected = new EventEmitter();


let clientsList = Array();

LoginServer.on("connection", function(socket)
{
    console.log("new connection");

    socket.on("data", function(data)
    {
        console.log(data.toString());

        let user = data.toString().split(',')[0];
        let pass = data.toString().split(',')[1];
        let resolution = data.toString().split(',')[2].replace(/\0/g, '');

        console.log(user + " | " + pass);
        DB.FindOne(user, pass, function(err, user)
        {
            if (err)
                console.error(err);
            else
            {
                if(user)
                {
                    const Id = objects.IdCreator(clientsList);
                    clientsList.push(new objects.Client(socket, user.nick, user.DB_Id, resolution, Id));
                    console.log(Id);
                    socket.write(JsonToSend.CreatePacket(200, Id, null));

                    console.log("OK");
                }
                else
                {
                    console.log("Wrong data!");
                    socket.write(JsonToSend.CreatePacket(404, null, null));
                }
            }

        });


    });
});

nrPlayersServer.on("connection", function(socket)
{

    MatchMaking.GameEmiter.on('start', function(number)
    {
        socket.send(JsonToSend.CreatePacket(200, null, number));

    });

})

switchRoomServer.on("connection", function(socket)
{

    switchRoomServer.getConnections(function(err, number)
    {
        PlayersConnected.emit('start', number);
    });

    if (socket.remoteAddress.substr(0, 7) == '::ffff:')
        socket.remoteAddress = socket.remoteAddress.substr(7);
    //matchmaking
    for(let i = 0; i < clientsList.length; i++)
    {
        if (clientsList[i].ip == socket.remoteAddress)
        {
            MetchMaking.add(clientsList[i]);
        }
    }

    setInterval(function()
    {
        socket.write(JsonToSend.CreatePacket(200, null, 0));
    }, 1000);



    MatchMaking.GameEmiter.on('start', function()
    {
        socket.write(JsonToSend.CreatePacket(200, null, 1));


    });


});



LoginServer.listen(8000, function(){
    console.log("port: 8000, LoginServer ");
});

switchRoomServer.listen(8003, function()
{
    console.log('switchRoomServer started at 127.0.0.1:8003');
});
