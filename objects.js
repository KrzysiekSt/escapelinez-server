class Client
{
	constructor(socket, Nick = "test", Id, resolution, DB_Id = "test")
	{
		this.State = "Online";
		this.states = ["Online", "Playing", "AFK"];
		this.Game = "None";
		this.Nick = Nick;
		this.Id = Id;
		this.DB_Id = DB_Id;
		this.Ip = socket.remoteAddress;
		this.socket = socket;
		this.resolution = resolution;
		if (this.Ip.substr(0, 7) == '::ffff:')
			this.Ip = this.Ip.substr(7);
		this.RoomId = null;
	}



}


class LineOb
{
	constructor(y)
	{
		this.y = y;
		this.speed = Math.floor(Math.random()*4+3);

		do
		{
			this.x[0] = Math.floor(Math.random()*(1921));
			this.x[1] = Math.floor(Math.random()*(1921));
		} while (Math.abs(x1 - x2) < 400 || Math.abs(x1 - x2) > 410);

	}
}


function IdCreator(tab)
{
	let NewId = 1;
	do
	{
		IsId = false;
		for (let i = 0; i < tab.length; i++)
		{

			if (tab[i].id == NewId)
			{
				IsId = true;
				NewId++;
				break;
			}
		}

	} while (IsId == true);

	return NewId;
}

module.exports =
{
	Client: Client,
	LineOb: LineOb,
	IdCreator: IdCreator,
};
