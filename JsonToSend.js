
function create(status, id, body)
{
    let value = JSON.stringify(
    {
        status: status,
        id: id,
        body: body

    });

    return Buffer.from(value, 'utf8');
}


module.exports =
{
    CreatePacket: create
}
