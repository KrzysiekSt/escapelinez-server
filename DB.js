const mongoose = require('mongoose');


mongoose.connect('mongodb://serwerkrolak.ddns.net/players', { useNewUrlParser: true });

mongoose.connection.once('open', function()
{
    console.log("connected to MongoDB");
});

mongoose.connection.on('err', function(err)
{
    console.log(err);
});


const UserSchema = new mongoose.Schema({
  	nick: 			String,
  	password: 		String,
  	email: 			String,
  	CreateDate: 	{ type: Date, default: Date.now },
  	LastLogin: 		{ type: Date, default: Date.now },
  	exp: 			{ type: Number, default: 0 },
  	Victories: 		{ type: Number, default: 0 },
  	Defeats: 		{ type: Number, default: 0 },
},
{
    collection: "user"
});


const User = mongoose.model('User', UserSchema);

// function Create(nick, password, email)
// {
// 	User.create
// 	({
// 		nick: 			nick,
// 		password: 		password,
// 		email: 			email,
// 	},
// 	function(err)
// 	{
// 		if (err)
//             return handleError(err);
// 		else
//         {
//             console.log(`Created new user ${nick}::${email}`);
//             return FindOne(nick, password);
//         }
//
// 	});
//
//
//
// }

function FindOne(nick, password, cb)
{
    User
        .findOne({ "nick": nick, "password": password })
            .exec(function (err, FoundUser)
            {
                cb(err, FoundUser);
            });

}

module.exports =
{
    // Create: Create,
    FindOne: FindOne,
};
